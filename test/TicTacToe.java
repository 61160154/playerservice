class TicTacToe {
  private char[][] table;
  private int turn;
  int row, col;


  TicTacToe(int row, int col) {
    this.row = row;
    this.col = col;
    turn = 1;
    int cnt = 1;
    table = new char[row][col];

    for(int i=row-1; i>=0; i--) {
      for(int j=0; j<col; j++) {
        table[i][j] = (char)(cnt + '0');
        cnt++;
      }
    }
  }


  char[][] getBoard() {
    return table;
  }

  char getBoardAt(int row, int col) {
    return table[row][col];
  }

  int getTurn() {
    return turn;
  }

  void setTurn(int turn) {
    this.turn = turn;
  }

  void showTurn() {
    if (turn % 2 == 1) {
        System.out.println("X Turn");
    } else {
        System.out.println("O Turn");
    }
  }

  void setBoard(int row, int col, char input) {
    table[row][col] = input;
    turn = turn+1;
  }

  public boolean isEnd() {
        if (checkRow() || checkColumn() || checkXcase()) {
            if (turn % 2 == 1) {
                System.out.println("O Win!\n");
            } else {
                System.out.println("X Win!\n");
            }
            return true;
        }
        if (turn == 9) {
            System.out.println("Draw!\n");
            return true;
        }
        return false;
    }

    private boolean checkRow() {
        for (int i = 0; i < 3; i++) {
            if (table[i][0] == table[i][1] && table[i][1] == table[i][2])
                return true;
        }
        return false;
    }

    private boolean checkColumn() {
        for (int i = 0; i < 3; i++) {
            if (table[0][i] == table[1][i] && table[1][i] == table[2][i])
                return true;
        }
        return false;
    }

    private boolean checkXcase() {
        if (table[0][0] == table[1][1] && table[1][1] == table[2][2])
            return true;

        if (table[0][2] == table[1][1] && table[1][1] == table[2][0])
            return true;

        return false;
    }


}